import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

import api from '@/api'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: null
  },
  mutations: {
    SET_USER (state, payload) {
      if (payload === null) {
        state.user = null
        return
      }
      state.user = {
        ...state.user,
        ...payload
      }
    }
  },
  actions: {
    async AUTHENTICATE (context) {
      const response = await api.get('/users/authenticate')
      const user = response.data
      if (user) {
        context.commit('SET_USER', user)
      }
    },
    async SIGN_OUT (context) {
      await api.post('/users/sign-out')
      context.commit('SET_USER', null)
      router.push('/')
    }
  },
  modules: {
  }
})

export default store
