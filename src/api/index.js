import axios from 'axios'
const { NODE_ENV } = process.env

axios.defaults.withCredentials = true
axios.defaults.baseURL = NODE_ENV === 'development' ? 'http://0.0.0.0:3000' : ''

export default axios
