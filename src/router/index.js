import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '@/store'

Vue.use(VueRouter)

async function protectedRoute (to, from, next) {
  if (!store.state.user) {
    return next({ name: 'SignIn' })
  }
  next()
}

function scrollBehavior (to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition
  } else {
    return { x: 0, y: 0 }
  }
}

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/sign-in',
    name: 'SignIn',
    component: () => import(/* webpackChunkName: "sign-in" */ '../views/SignIn.vue')
  },
  {
    path: '/sign-up',
    name: 'SignUp',
    component: () => import(/* webpackChunkName: "sign-up" */ '../views/SignUp.vue')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "settings" */ '../views/settings/Settings.vue'),
    beforeEnter: protectedRoute,
    children: [
      {
        path: '',
        alias: 'personal',
        component: () => import(/* webpackChunkName: "settings/personal" */ '../views/settings/SettingsPersonal.vue')
      },
      {
        path: 'account',
        component: () => import(/* webpackChunkName: "settings/account" */ '../views/settings/SettingsAccount.vue')
      }
    ]
  },
  {
    path: '/verify/:token?',
    name: 'Verify',
    component: () => import(/* webpackChunkName: "verify" */ '../views/Verify.vue')
  },
  {
    path: '/reset-password/:resetPasswordToken?',
    name: 'ResetPassword',
    component: () => import(/* webpackChunkName: "reset-password" */ '../views/ResetPassword.vue')
  },
  {
    path: '/styleguide',
    name: 'Styleguide',
    component: () => import(/* webpackChunkName: "styleguide" */ '../views/Styleguide.vue')
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "not-found" */ '../views/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior
})

export default router
